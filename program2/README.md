This folder contains the first project related to DuckChat.

For this project, we needed to have a client server architecture where there is a tree of servers communicating with each other. There can be clients on any of the servers in the tree and messages will get properly routed.

**Usage Guide:**

First, run make

In order to initialize the server, run /server server1_ip server1_port server2_ip server2_port ....  for each server that is connected.

In order to initialize the client, run /client server ip server port user name


The start_servers shell script will launch a tree of servers to test how the packets get sent between servers.