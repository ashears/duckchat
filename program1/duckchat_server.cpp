/* CIS 432 Computer Networks Fall 2017
 * University of Oregon
 * Ashton Shears and Clayton Kilmer
 * 
 * This program is a chat server over UDP
 */
#include "duckchat.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include<netdb.h> 

#include <utility>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

using namespace std;
map<string, pair<string, sockaddr_in>> userIpPorts;
map<string, set<string>> channelUserIpPorts;

void closeEmptyChannels() {
    for (auto it = channelUserIpPorts.begin(); it != channelUserIpPorts.end(); it++) {
        if ((it->second).size() == 0) {
            channelUserIpPorts.erase(it->first);
        }
    }
}

int main(int argc, char **argv){
    if (argc != 3){
        perror("Duckchat server requires exactly two command-line arguments\n");
        return 1;
    }

    // hostname to ip from from binarytides.com/hostname-to-ip-address-c-sockets-linux
    struct hostent* he;
    struct in_addr** addr_list;
    char ip[100];
    char ipA[100];
         
    if ((he = gethostbyname(argv[1])) == NULL) {
        perror("Could not get hostname\n");
        return -1;
    }
 
    addr_list = (in_addr**) he->h_addr_list;
     
    for(int i = 0; addr_list[i] != NULL; i++) {
        //Return the first one;
        strcpy(ip, inet_ntoa(*addr_list[i]));
        break;
    }

    int serverPort = atoi(argv[2]);
    sockaddr_in myAddress;
    myAddress.sin_family = AF_INET;
    myAddress.sin_port = htons(serverPort);
    // myAddress.sin_addr.s_addr = inet_addr(ip);
    inet_pton(AF_INET, ip, &myAddress.sin_addr);
    // cout << "from myAddress: IP " << myAddress.sin_addr.s_addr << " port " << myAddress.sin_port << endl;
    

    int serverSocket = socket(AF_INET, SOCK_DGRAM, 0);//CREATE IPV4 UDP SOCKET
    if (serverSocket < 0) {
        perror("Could not create socket\n");
        return -1;
    }

    if (bind(serverSocket, (sockaddr*)&myAddress, sizeof(myAddress)) != 0) {
        perror("Bind failed!\n");
        return -1;
    }

    socklen_t fromlen;
    sockaddr_in src_address;

    // init request structs
    request* gen_request = new request(); 
    memset(gen_request, 0, sizeof(request));

    request_login* login_request = new request_login(); 
    memset(login_request, 0, sizeof(request_login));

    request_join* join_request = new request_join(); 
    memset(join_request, 0, sizeof(request_who));

    request_leave* leave_request = new request_leave(); 
    memset(leave_request, 0, sizeof(request_leave));

    request_say* say_request = new request_say();
    memset(say_request, 0, sizeof(request_say)); 

    request_who* who_request = new request_who(); 
    memset(who_request, 0, sizeof(request_who));

    // init text structs
    text_say* say_text = new text_say(); 
    memset(say_text, 0, sizeof(text_say));

    say_text->txt_type = TXT_SAY;
    text_error* error_text = new text_error(); 
    memset(error_text, 0, sizeof(text_error));

    error_text->txt_type = TXT_ERROR;
    text_list* list_text = NULL; // have to be dynamically allocated
    text_who* who_text = NULL; // have to be dynamically allocated


    string ipPort;
    string errorString;
    char ipAddress[INET_ADDRSTRLEN];

    while(1) {
        if (recvfrom(serverSocket, gen_request, sizeof(request_say), 0, (sockaddr*)&src_address, &fromlen) < 0) {
            cout << "recvfrom failed, continuing." << endl;
            continue;
        }
        printf("req %d\n", gen_request->req_type);

        stringstream ipStream;
        stringstream errorStream;
        inet_ntop(AF_INET, &(src_address.sin_addr), ipAddress, INET_ADDRSTRLEN);

        ipPort = ipAddress;
        ipPort += ':';
        ipStream << ipPort << (&src_address)->sin_port;
        ipStream >> ipPort;

        auto it = userIpPorts.find(ipPort);
        if (it == userIpPorts.end() && gen_request->req_type != REQ_LOGIN) {
            cout << "ERROR! User @ " << ipPort << " has not logged in yet!" << endl;
            errorStream << "User @ " << ipPort << " has not logged in yet!";
            errorString = errorStream.str();
            strcpy(error_text->txt_error, errorString.c_str());
            sendto(serverSocket, error_text, sizeof(text_error), 0, (sockaddr*)&src_address, sizeof(sockaddr_in));
        }
        else if (gen_request->req_type == REQ_LOGIN) {
            login_request = (request_login*)gen_request;
            cout << "User " << login_request->req_username << " @ " << ipPort << " logs in." << endl;
            userIpPorts[ipPort].first = string(login_request->req_username);
            memcpy((void*)&(userIpPorts[ipPort].second), (void*)&src_address, sizeof(sockaddr_in));
        } 
        else if (gen_request->req_type == REQ_LOGOUT) {
            cout << userIpPorts[ipPort].first << " @ " << ipPort << " logging out." << endl;

            // for each channel remove the userIpPort from the set of userIpPorts subscribbed to the channel
            for (auto it = channelUserIpPorts.begin(); it != channelUserIpPorts.end(); it++) {
                it->second.erase(ipPort);
            }
            userIpPorts.erase(ipPort);
            closeEmptyChannels();
        } 
        else if (gen_request->req_type == REQ_JOIN) {
            join_request = (request_join*)gen_request;
            cout << userIpPorts[ipPort].first << " joins " << join_request->req_channel << endl;
            channelUserIpPorts[join_request->req_channel].insert(ipPort);
        }
        else if (gen_request->req_type == REQ_LEAVE) {
            leave_request = (request_leave*)gen_request;
            cout << userIpPorts[ipPort].first << " @ " << ipPort << " leaves " << leave_request->req_channel << endl;
            channelUserIpPorts[leave_request->req_channel].erase(ipPort);

            if (channelUserIpPorts[leave_request->req_channel].size() == 0) {
                channelUserIpPorts.erase(leave_request->req_channel);
            }
        }  
        else if (gen_request->req_type == REQ_SAY) { 
            say_request = (request_say*)gen_request;
            
            auto it = channelUserIpPorts.find(say_request->req_channel);
            if (it != channelUserIpPorts.end() && (it->second).find(ipPort) != (it->second).end()) { // if channel exists and user subscribbed to it
                strcpy(say_text->txt_channel, say_request->req_channel);
                strcpy(say_text->txt_username, (userIpPorts[ipPort].first).c_str());
                strcpy(say_text->txt_text, say_request->req_text);
                cout << "server: " << "[" << say_text->txt_channel << "]" << "[" << say_text->txt_username << "] " << say_text->txt_text << endl;;

                // for each user subscribed to say_request->req_channel send say_text to. 
                set<string> channelUsers = channelUserIpPorts[say_request->req_channel];
                for (auto it = channelUsers.begin(); it != channelUsers.end(); it++) {
                    sendto(serverSocket, say_text, sizeof(text_say), 0, (sockaddr*)&(userIpPorts[*it].second), sizeof(sockaddr_in));
                }
            } else {
                cout << "ERROR! Channel " << say_request->req_channel << " does not exist or user has not subscribed to channel!" << endl;
                errorStream << "Channel " << say_request->req_channel << " does not exist or user has not subscribed to channel!";                
                strcpy(error_text->txt_error, errorString.c_str());
                sendto(serverSocket, error_text, sizeof(error_text), 0, (sockaddr*)&src_address, sizeof(sockaddr_in));
            } 
        }
        else if (gen_request->req_type == REQ_LIST) { 

            int tlSize = sizeof(text_list) + (channelUserIpPorts.size() * sizeof(channel_info));
            list_text = (text_list*)realloc(list_text, tlSize);
            list_text->txt_type = TXT_LIST;
            list_text->txt_nchannels = channelUserIpPorts.size();

            int i = 0;
            cout << "/list: ";
            for (auto it = channelUserIpPorts.begin(); it != channelUserIpPorts.end(); it++) {
                strcpy(list_text->txt_channels[i].ch_channel, it->first.c_str());
                cout << list_text->txt_channels[i].ch_channel << ", ";             
                i++;
            }
            cout << endl;
            sendto(serverSocket, list_text, tlSize, 0, (sockaddr*)&src_address, sizeof(sockaddr_in));
        }
        else if (gen_request->req_type == REQ_WHO) { 
            who_request = (request_who*)gen_request;
            
            auto it = channelUserIpPorts.find(who_request->req_channel);
            if (it != channelUserIpPorts.end()) {
                set<string> channelUsers = it->second;

                int twSize = sizeof(text_who) + (channelUsers.size() * sizeof(user_info));
                who_text = (text_who*)realloc(who_text, twSize); 
                who_text->txt_type = TXT_WHO;
                who_text-> txt_nusernames = channelUsers.size();
                strcpy(who_text->txt_channel, who_request->req_channel);

                int i = 0;
                cout << "/who " << who_request->req_channel << ": ";
                for (auto it = channelUsers.begin(); it != channelUsers.end(); it++) {
                    strcpy(who_text->txt_users[i].us_username, (userIpPorts[*it].first).c_str());
                    cout << who_text->txt_users[i].us_username << ", ";
                    i++;
                }
                cout << endl;
                sendto(serverSocket, who_text, twSize, 0, (sockaddr*)&src_address, sizeof(sockaddr_in));
            }
        }
        else {
            cout << "ERROR! Unknown request type." << endl;
            errorStream << "Unknown request type.";
            strcpy(error_text->txt_error, errorString.c_str());
            sendto(serverSocket, error_text, sizeof(error_text), 0, (sockaddr*)&src_address, sizeof(sockaddr_in));
        }
    }
}
