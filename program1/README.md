This folder contains the first project related to DuckChat.

For this project, we needed to have a single server that allows multiple clients to communicate with each other.

**Usage Guide:**

First, run make

In order to initialize the server, run /server [[server ip]] [[server port]]

In order to initialize the client, run /client [[server ip]] [[server port]] [[user name]]
